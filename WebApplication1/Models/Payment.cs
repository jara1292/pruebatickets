﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Table("PAY_PAYMENT")]
    public class Payment
    {
        public string TIC_ID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required(ErrorMessage = "Debes ingresar este campo")]
        public int PAY_ID { get; set; }

        [Required(ErrorMessage = "Debes ingresar este campo")]
        //[DataType(DataType.Currency,ErrorMessage ="Ingresar formato valido")]
        public decimal PAY_TOTAL { get; set; }
        public int PAY_CANCELED { get; set; }

        public virtual Ticket Ticket { get; set; }
    }
}