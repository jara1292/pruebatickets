﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Table("PAY_TICKET")]
    public class Ticket
    {
        [Key]
        [Required(ErrorMessage = "Debes ingresar este campo")]
        public string TIC_ID { get; set; }

        [Required(ErrorMessage = "Debes ingresar este campo")]
        //[DataType(DataType.Currency,ErrorMessage ="Ingresar formato correcto")]
        public decimal TIC_TOTALAMOUNT { get; set; }

        
        public virtual ICollection<Payment> Payments { get; set; }
    }
}