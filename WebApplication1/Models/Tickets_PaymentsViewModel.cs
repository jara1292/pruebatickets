﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Tickets_PaymentsViewModel
    {
        public string TIC_ID { get; set; }
        public decimal TIC_TOTALAMOUNT { get; set; }

        public decimal PAGADO { get; set; }
        public decimal FALTA { get; set; }
    }
}