﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class DataContext:DbContext
    {
        public DataContext() : base("Prueba")
        {

        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Payment> Payments { get; set; }
    }
}