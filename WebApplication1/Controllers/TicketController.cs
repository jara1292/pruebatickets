﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TicketController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Tickets
        public ActionResult Index()
        {
            //Ejecutar Procedimiento almacenado para traer los datos de la BD
            var tickets = db.Database.SqlQuery<Tickets_PaymentsViewModel>("EXEC SP_TPAGOS");
            return View(tickets);
        }

        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(Ticket ticket)
        {

            if (ModelState.IsValid)
            {
                //Validar si ya fue registrado el ticket
                var validaticket = db.Tickets.Find(ticket.TIC_ID);
                if (validaticket != null)
                {
                    ViewBag.Mensaje = "Este ticket ya fue ingresado";
                    return View(ticket);
                }

                db.Tickets.Add(ticket);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }
                
                return RedirectToAction("Index");
            }
            return View(ticket);
            
        }


        public ActionResult Edit(string Id)
        {
            var ticket = db.Tickets.Find(Id);
            return View(ticket);
        }


        [HttpPost]
        public ActionResult Edit(Ticket ticket)
        {
            var reg = db.Tickets.Find(ticket.TIC_ID);
            reg.TIC_TOTALAMOUNT = ticket.TIC_TOTALAMOUNT;
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
            
            return RedirectToAction("Index");
        }

        public ActionResult Delete(string Id)
        {
            return View(db.Tickets.Find(Id));
        }


        [HttpPost]
        public ActionResult Delete(Ticket ticket2)
        {
            //Eliminar pagos correspondientes al ticket
            var pays = db.Payments.Where(x=>x.TIC_ID == ticket2.TIC_ID).ToList();
            db.Payments.RemoveRange(pays);

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
            
            return RedirectToAction("Index");
        }


        public ActionResult Details(string Id)
        {
            return View(db.Tickets.Find(Id));
        }
    }
}