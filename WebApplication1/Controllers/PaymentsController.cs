﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PaymentsController : Controller
    {
        private DataContext db = new DataContext();

        

        public ActionResult Create()
        {
            //Combo de tickets
            var tickets = new SelectList(db.Tickets, "TIC_ID", "TIC_ID");
            ViewBag.TIC_ID = tickets;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Payment payment)
        {
            //Combo de tickets
            ViewBag.TIC_ID = new SelectList(db.Tickets, "TIC_ID", "TIC_ID");
            if (ModelState.IsValid)
            {
                //Validar si pago ya fue dado de alta
                var pago = db.Payments.Find(payment.PAY_ID);
                if (pago != null)
                {
                    ViewBag.Mensaje = "Ya fue dado de alta este pago";
                    return View(payment);
                }


                //Validar que monto del ticket y monto de los pagos coincidan o no sea mayor
                var ticket = db.Tickets.Find(payment.TIC_ID);
                var pagos = db.Payments.Where(x => x.TIC_ID == payment.TIC_ID);

                var total_pagos = pagos.Count()==0 ? 0 : pagos.Sum(p=>p.PAY_TOTAL);

                //SI ya esta todo pagado
                if (ticket.TIC_TOTALAMOUNT == total_pagos)
                {
                    ViewBag.Mensaje = "Este ticket ya esta pagado";
                    return View(payment);
                }
                //Si es mayor los pagos al monto del ticket
                else if ((total_pagos + payment.PAY_TOTAL)>ticket.TIC_TOTALAMOUNT )
                {
                    ViewBag.Mensaje = "El monto ingresado supera el monto total del ticket";
                    return View(payment);
                }

                db.Payments.Add(payment);
                db.SaveChanges();

                try
                {
                    return RedirectToAction("Index", "Ticket", null);
                }
                catch (Exception)
                {

                    throw;
                }
                
            }
            
            return View(payment);
        }


    }
}